/* CAN Loopback Example
 * This example sends a message once a second and receives that message
 *   no CAN bus is required.  This example will test the functionality 
 *   of the protocol controller, and connections to it.
 *   
 *   Written By: Cory J. Fowler - October 5th 2016
 */
//#define TRACE_ON

#ifdef TRACE_ON
  #define LOG_TRACE(msg) Serial.println(msg)
#else
  #define LOG_TRACE(msg) ;
#endif

#include <SPI.h>
#include <mcp_can.h>
#include "MSG_defn.h"
#include <Adafruit_GFX.h>
#include <Wire.h>
#include <Adafruit_ILI9341.h>

#define MY_ID 2

// CAN RX Variables
uint32_t rxId;
uint8_t ext;
unsigned char len;
unsigned char rxBuf[8];
unsigned char txBuf[8];

#define TFT_CS 10
#define TFT_DC 9
Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC);

uint8_t tables[3*64];

// Serial Output String Buffer
char msgString[128];

// CAN0 INT and CS
#define CAN0_INT 2  // Set INT to pin 2
#define CAN0_CS  8  // Set CS to pin 8
MCP_CAN CAN0(CAN0_CS);

void setup()
{
  Serial.begin(115200);  // CAN is running at 500,000BPS; 115,200BPS is SLOW, not FAST, thus 9600 is crippling.
  
  tft.begin();
  // origin = left,top landscape (USB right lower)
  tft.setRotation(3); 
  tft.fillScreen(ILI9341_BLACK);
  tft.setCursor(0,0);
  tft.setTextColor(ILI9341_WHITE);
  tft.setTextSize(2);
  tft.println("MegaGauge v1.0");
  
  // Initialize MCP2515 running at 16MHz with a baudrate of 500kb/s and the masks and filters disabled.
  if(CAN0.begin(MCP_ANY, CAN_500KBPS, MCP_16MHZ) == CAN_OK)
    Serial.println("MCP2515 Initialized Successfully!");
  else
    Serial.println("Error Initializing MCP2515...");
  
  // Since we do not set NORMAL mode, we are in loopback mode by default.
  CAN0.setMode(MCP_NORMAL);
  
  pinMode(CAN0_INT, INPUT);// Configuring pin for CAN interrupt input
  
  tft.setTextSize(1);
  tft.setCursor(0,20);
  tft.print("PORT0");
}

void loop()
{
  if(!digitalRead(CAN0_INT))
  {
    CAN0.readMsgBuf(&rxId, &ext, &len, rxBuf);              // Read data: len = data length, buf = data byte(s)
    
    if(ext){
      MS_HDR_t* hdr = reinterpret_cast<MS_HDR_t*>(&rxId);
      if(hdr->toId==MY_ID){
        handleExtended(hdr,len,rxBuf);
      } else {
        Serial.println("Request not meant for me!!!");
      }// if
    } else {
      handleStandard(rxId,len,rxBuf);
    }
  }
  uint8_t* ports = &tables[16];
//  Serial.println(ports[0]);
  ports[1]--;
  ports[2]++;
  
  drawPort(ports[0],33,20,ILI9341_GREEN,ILI9341_RED);
  
  uint16_t adc0 = analogRead(0);
  reinterpret_cast<uint16_t*>(tables)[0] = bswap16(adc0);
}

void drawPort(const uint8_t & portVal, int16_t x, int16_t y, uint16_t activeColor, uint16_t inactiveColor)
{
  const int16_t w = 5;
  
  for(uint8_t bb=0; bb<8; bb++)
  {
    if(1<<bb&portVal)
    {
      tft.fillRect((w+1)*(7-bb)+x,y,w,w,activeColor);
    } else {
      tft.fillRect((w+1)*(7-bb)+x,y,w,w,inactiveColor);
    }
  }
}

void handleStandard(uint32_t id, uint8_t length, uint8_t* data)
{
  LOG_TRACE("handleStandard");
  
  tft.setTextColor(ILI9341_WHITE,ILI9341_BLACK);
  tft.setTextSize(1);
  switch(id){
    case MSG00_ID:{
      #define RPM_X 0
      #define RPM_Y 60
      MSG00_t* msg = reinterpret_cast<MSG00_t*>(data);
      sprintf(msgString,"time = %d\n",bswap16(msg->seconds));
      Serial.print(msgString);
      sprintf(msgString,"pw1 = %d\n",bswap16(msg->pw1));
      Serial.print(msgString);
      sprintf(msgString,"pw2 = %d\n",bswap16(msg->pw2));
      Serial.print(msgString);
      sprintf(msgString,"rpm = %d\n",bswap16(msg->rpm));
      tft.setCursor(RPM_X,RPM_Y);
      tft.print(msgString);
      break;
    }
    case MSG02_ID:{
      #define CLT_X 0
      #define CLT_Y 70
      MSG02_t* msg = reinterpret_cast<MSG02_t*>(data);
      int16_t baro = bswap16(msg->baro);
      sprintf(msgString,"baro = %d.%d\n",baro/10,abs(baro%10));
      Serial.print(msgString);
      int16_t map = bswap16(msg->map);
      sprintf(msgString,"map = %d.%d\n",map/10,abs(map%10));
      int16_t clt = bswap16(msg->clt);
      sprintf(msgString,"clt_F = %d.%d\n",clt/10,abs(clt%10));
      tft.setCursor(CLT_X,CLT_Y);
      tft.print(msgString);
      break;
    }
    case MSG03_ID:{
      MSG03_t* msg = reinterpret_cast<MSG03_t*>(data);
      int16_t tps = bswap16(msg->tps);
      sprintf(msgString,"tps = %d.%d\n",tps/10,abs(tps%10));
      tft.setCursor(0,30);
      tft.print(msgString);
      int16_t batt = bswap16(msg->batt);
      sprintf(msgString,"batt = %d.%d\n",batt/10,batt%10);
      tft.setCursor(0,40);
      tft.print(msgString);
      break;
    }
    case MSG10_ID:{
      #define STAT2_X 0
      #define STAT2_Y 50
      MSG10_t* msg = reinterpret_cast<MSG10_t*>(data);
      sprintf(msgString,"status2 = 0x%0.2x\n",msg->status2);
      tft.setCursor(STAT2_X,STAT2_Y);
      tft.print(msgString);
      break;
    }
    default:{
      break; 
    }
  }//s witch
}

void handleExtended(const MS_HDR_t* hdr, const uint8_t length, uint8_t* data)
{
//  sprintf(
//    msgString,
//    "table: %d toId: %d fromId: %d type: %d offset: %d\n",
//    hdr->tableL|hdr->tableH<<4,
//    hdr->toId,
//    hdr->fromId,
//    hdr->type,
//    hdr->offset);
//  Serial.print(msgString);
  
  switch(hdr->type){
    case MSG_CMD:
      handleCommand(hdr,length,data);
      break;
    case MSG_REQ:
      handleRequest(hdr,data);
      break;
    default:
      Serial.print("Unimplemented MSG type: ");
      Serial.println(hdr->type);
      break;
  }// switch
}

void handleCommand(const MS_HDR_t* hdr, const uint8_t length, uint8_t* data)
{
  LOG_TRACE("COMMAND");
  
  uint8_t table = hdr->tableL|hdr->tableH<<4;
  
  memcpy(tables+64*table+hdr->offset,data,length);
}

void handleRequest(const MS_HDR_t* hdr, uint8_t* reqData)
{
  LOG_TRACE("REQUEST");
  
  MSG_REQ_t* req = reinterpret_cast<MSG_REQ_t*>(reqData);
  uint8_t reqTable = hdr->tableL|hdr->tableH<<4;
  
  uint16_t rspOffset = req->rspOffsetH<<3|req->rspOffsetL;
//  sprintf(
//    msgString,
//    "resTable: %d resLen: %d resOffset: %d\n",
//    req->rspTable,
//    req->rspLength,
//    rspOffset);
//  Serial.print(msgString);
  
  MS_HDR_t rspHdr;
  rspHdr.toId = hdr->fromId;
  rspHdr.fromId = MY_ID;
  rspHdr.type = MSG_RSP;
  rspHdr.tableL = req->rspTable&0xF;
  rspHdr.tableH = req->rspTable>>4;
  rspHdr.offset = rspOffset;
  
  CAN0.sendMsgBuf(
    *reinterpret_cast<uint32_t*>(&rspHdr),
    true,
    req->rspLength,
    (uint8_t*)tables+(64*reqTable)+hdr->offset);
}

/*********************************************************************************************************
  END FILE
*********************************************************************************************************/
