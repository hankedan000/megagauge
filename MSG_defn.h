#ifndef MSG_DEFN_H
#define MSG_DEFN_H

#define bswap16(x) __builtin_bswap16(x)

#define MSG_CMD     0
#define MSG_REQ     1
#define MSG_RSP     2
#define MSG_XSUB    3
#define MSG_BURN    4
#define OUTMSG_REQ  5
#define OUTMSG_RSP  6
#define MSG_XTND    7
#define MSG_FWD     8
#define MSG_CRC     9
#define MSG_REQX    12
#define MSG_BURNACK 14


struct MS_HDR_t{
  uint8_t          : 2;
  uint8_t   tableH : 1;
  uint8_t   tableL : 4;
  uint8_t   toId   : 4;
  uint8_t   fromId : 4;
  uint8_t   type   : 3;
  uint16_t  offset : 11;
  uint8_t          : 3;
}__attribute__((packed));

struct MSG_REQ_t{
  uint8_t   rspTable   : 5;
  uint8_t              : 3;
  uint8_t   rspOffsetH : 8;
  uint8_t   rspLength  : 4;
  uint8_t              : 1;
  uint8_t   rspOffsetL : 3;
}__attribute__((packed));

#define MSG_BASE  1520

#define MSG00_ID  MSG_BASE+0
struct MSG00_t{
  uint16_t  seconds: 16;
  uint16_t  pw1    : 16;
  uint16_t  pw2    : 16;
  uint16_t  rpm    : 16;
};

#define MSG02_ID  MSG_BASE+2
struct MSG02_t{
  int16_t   baro   : 16;
  int16_t   map    : 16;
  int16_t   mat    : 16;
  int16_t   clt    : 16;
};

#define MSG03_ID  MSG_BASE+3
struct MSG03_t{
  int16_t  tps     : 16;
  int16_t  batt    : 16;
  int16_t  afr1    : 16;
  int16_t  afr2    : 16;
};

#define MSG10_ID  MSG_BASE+10
struct MSG10_t{
  uint8_t  status1  : 8;
  uint8_t  status2  : 8;
  uint8_t  status3  : 8;
  uint8_t  status4  : 8;
  uint16_t status5  : 16;
  uint8_t  status6  : 8;
  uint8_t  status7  : 8;
};

#endif
