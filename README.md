# MegaGauge
An Arduino based gauge for the open source engine management system, [MegaSquirt](http://megasquirt.info/).

## Required Hardware
* [MCP2515 CAN Board](http://www.ebay.com/sch/mcp2515)
* [2.8" TFT Touch Shield](https://www.adafruit.com/product/1947)

## Required Libraries
This project uses external Arduino libraries to interface to the LCD and CAN bus hardware. Simply follow the links below, and install the libraries into the "libraries" folder of your Arduino sketchbook directory.

* [MCP_CAN_lib](https://github.com/coryjfowler/MCP_CAN_lib)
* [Adafruit_ILI9341](https://github.com/adafruit/Adafruit_ILI9341)
* [Adafruit_GFX](https://github.com/adafruit/Adafruit-GFX-Library)

## References
* [MCP2515 datasheet](http://ww1.microchip.com/downloads/en/DeviceDoc/20001801H.pdf)
* [MegaSquirt 29bit CAN protocol](http://www.msextra.com/doc/pdf/Megasquirt_29bit_CAN_Protocol-2015-01-20.pdf)
* [MegaSquirt CAN Broadcast](http://www.msextra.com/doc/pdf/Megasquirt_CAN_Broadcast.pdf)